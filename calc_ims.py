# coding: utf-8
import h5py
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.optimize
from tqdm import tqdm

def func_mz(args):
    a1, k1 = args
    mz1 = (a1 * idx + k1) ** 2
    return mz1 - mz

def func(args):
    a1, k1 = args
    sqrt_mz1 = a1 * idx + k1
    return sqrt_mz1 - sqrt_mz

result = []
with h5py.File('sgs/tof/napedro_l120417_010_sw.spech5', 'r') as f:
    for window in tqdm(f.keys()):
        full_mz = list()
        full_sqrt_mz = list()
        full_idx = list()
        for i, k in enumerate(f[window].keys()):
            mz = f[window][k]['MASS_OVER_CHARGE'][()]
            sqrt_mz = np.sqrt(mz)
            delta_sqrt_mz = np.diff(sqrt_mz)
            a = delta_sqrt_mz.min()
            idx = np.cumsum(np.round(delta_sqrt_mz / a).astype(int))
            idx = np.array([0] + list(idx)) + np.round(sqrt_mz[0] / a).astype(int)
            full_mz.extend(list(mz))
            full_sqrt_mz.extend(list(sqrt_mz))
            full_idx.extend(list(idx))
            if i == 100:
                break
        mz = np.array(full_mz)
        sqrt_mz = np.array(full_sqrt_mz)
        idx = np.array(full_idx)

        r = scipy.optimize.least_squares(func, [a, 0.0], method='lm')
        ppm = 1e6 * func_mz(r.x) / mz
        r = {
            'Window': window,
            'PPM_MAD': pd.DataFrame(ppm).mad().iloc[0],
            'IMSAlpha': r.x[0],
            'IMSBeta': r.x[1],
            'IMSGamma': idx[0],
        }
        result.append(r)
result = pd.DataFrame(result).set_index('Window')

result.PPM_MAD.plot(ls='o')
plt.show()

