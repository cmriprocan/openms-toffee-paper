MATCH
    (c_scan:LAKE_ARTIFACT_CLASS{name:"ScanSwath"})<-[:HAS_CLASS]-(a_scan:ARTIFACT)
        <-[:DEFINES]-(wai_scan:WORKFLOW_ARTIFACT_INSTANCE)
        <-[:INPUT]-(wi_conversion:WORKFLOW_INSTANCE)
        -[:INPUT]->(wai_wiff:WORKFLOW_ARTIFACT_INSTANCE)
        -[:DEFINES]->(a_wiff:ARTIFACT)
        -[:HAS_CLASS]->(c_wiff:LAKE_ARTIFACT_CLASS{name:"WiffSwath"}),
    (a_scan)<-[:OUTPUT_ARTIFACT]-(wai_scan1:WORKFLOW_ARTIFACT_INSTANCE)
        <-[:OUTPUT]-(wi:WORKFLOW_INSTANCE)
        -[:OUTPUT]->(wai_wiff1:WORKFLOW_ARTIFACT_INSTANCE)
        -[:OUTPUT_ARTIFACT]->(a_wiff),
    (wi)-[:HAS_BINDING]->(b:BINDING_INSTANCE{label:"filename"})
where
    wi_conversion:SUCCEEDED AND wi:SUCCEEDED
    
optional match
    (wi_conversion)-[:OUTPUT]->(wai_tof:WORKFLOW_ARTIFACT_INSTANCE)
        -[:OUTPUT_ARTIFACT]->(a_tof:ARTIFACT)
        -[:HAS_CLASS]->(c_tof:LAKE_ARTIFACT_CLASS{name:"RawTof"})
        
optional match
    (c_tof_dscore:LAKE_ARTIFACT_CLASS)<-[:HAS_CLASS]-(a_tof_dscore:ARTIFACT)
        <-[:OUTPUT_ARTIFACT]-(wai_tof_dscore:WORKFLOW_ARTIFACT_INSTANCE)
        <-[:OUTPUT]-(wi_tof_pipeline:WORKFLOW_INSTANCE)
        -[:INPUT]->(wai_tof1:WORKFLOW_ARTIFACT_INSTANCE)
        -[:DEFINES]->(a_tof)
where c_tof_dscore.name = "SwathScoresSql" OR c_tof_dscore.name = "SwathScores"

optional match
    (wi_conversion)-[:OUTPUT]->(wai_mzml:WORKFLOW_ARTIFACT_INSTANCE)
        -[:OUTPUT_ARTIFACT]->(a_mzml:ARTIFACT)
        -[:HAS_CLASS]->(c_mzml:LAKE_ARTIFACT_CLASS{name:"RawMzmlSwath"})
optional match
    (c_mzml_dscore:LAKE_ARTIFACT_CLASS)<-[:HAS_CLASS]-(a_mzml_dscore:ARTIFACT)
        <-[:OUTPUT_ARTIFACT]-(wai_mzml_dscore:WORKFLOW_ARTIFACT_INSTANCE)
        <-[:OUTPUT]-(wi_mzml_pipeline:WORKFLOW_INSTANCE)
        -[:INPUT]->(wai_mzml1:WORKFLOW_ARTIFACT_INSTANCE)
        -[:DEFINES]->(a_mzml)
where c_mzml_dscore.name = "SwathScoresSql" OR c_mzml_dscore.name = "SwathScores"

RETURN
    b.value as InjectionName,
    wi.uid as IngestionWorkflowUid,
    wi.createdAt as IngestionDate,
    a_wiff.uid as WiffUid,
    a_scan.uid as ScanUid,
    (a_wiff.filesize + a_scan.filesize) / 1024.0 / 1024.0 as WiffFilesizeMB,
    wi_conversion.uid as ConversionWorkflowUid,
    wi_conversion.createdAt as ConversionDate,
    (wi_conversion.completedAt - wi_conversion.startedAt) / 60000.0 as ConversionRuntimeMinutes,
    a_tof.uid as TofUid,
    (a_tof.filesize) / 1024.0 / 1024.0 as TofFilesizeMB,
    wi_tof_pipeline.uid as TofPipelineWorkflowUid,
    wi_tof_pipeline.createdAt as TofPipelineDate,
    (wi_tof_pipeline.completedAt - wi_tof_pipeline.startedAt) / 60000.0 as TofPipelineRuntimeMinutes,
    a_mzml.uid as MzmlUid,
    (a_mzml.filesize) / 1024.0 / 1024.0 as MzmlFilesizeMB,
    wi_mzml_pipeline.uid as MzmlPipelineWorkflowUid,
    wi_mzml_pipeline.createdAt as MzmlPipelineDate,
    (wi_mzml_pipeline.completedAt - wi_mzml_pipeline.startedAt) / 60000.0 as MzmlPipelineRuntimeMinutes
