import os

import pandas as pd


def getsize(fname):
    if os.path.isfile(fname):
        return os.path.getsize(fname) / 1024 / 1024
    return pd.np.NaN


def load_sgs_metadata():
    basedir = 'sgs'
    basename = basedir
    metadata = pd.read_csv(f'{basedir}/{basename}-metadata.tsv', sep='\t')
    mask1 = metadata.Background == 'Human'
    mask2 = metadata.TechnicalReplicate == 1
    metadata = metadata[mask1 & mask2]
    return basedir, basename, metadata.sort_values('Dilution', ascending=False).reset_index()


def load_roest2016_metadata():
    basedir = 'roest2016'
    basename = basedir
    metadata = pd.read_csv(f'{basedir}/{basename}-metadata.tsv', sep='\t')
    return basedir, basename, metadata.sort_values('InjectionName', ascending=False).reset_index()


def load_procan90_metadata():
    basedir = 'procan90'
    basename = basedir
    metadata = pd.DataFrame([{'InjectionName': 'ProCan90-M0{}-01'.format(m), 'Machine': m} for m in range(1, 7)])
    return basedir, basename, metadata


def main(metadata_func):
    basedir, basename, metadata = metadata_func()
    mzml_input_dirs = ['mzML/' + a for a in 'a,b,c,d,e,f,g'.split(',')]
    tof_input_dirs = ['tof/a', 'tof/e']
    filesizes = []
    for _, row in metadata.iterrows():
        sizes = {
            'InjectionName': row.InjectionName,
            'Wiff': getsize(f'{basedir}/wiff/{row.InjectionName}.wiff'),
            'Scan': getsize(f'{basedir}/wiff/{row.InjectionName}.wiff.scan'),
            'mz5': getsize(f'{basedir}/mz5/{row.InjectionName}.mz5'),
        }
        for input_dir in mzml_input_dirs:
            key = input_dir.split('/')[-1]
            sizes[input_dir] = getsize(f'{basedir}/{input_dir}/{row.InjectionName}.{key}.mzML')
        for input_dir in tof_input_dirs:
            key = input_dir.split('/')[-1]
            sizes[input_dir] = getsize(f'{basedir}/{input_dir}/{row.InjectionName}.{key}.tof')
        filesizes.append(sizes)

    filesizes = pd.DataFrame(filesizes)
    filesizes['Vendor'] = filesizes.Wiff + filesizes.Scan
    filesizes.to_csv(f'{basedir}/{basename}-filesizes.tsv', sep='\t', index=False)


if __name__ == '__main__':
    main(load_sgs_metadata)
    main(load_roest2016_metadata)
    main(load_procan90_metadata)
