import argparse
import errno
import hashlib
import logging
import multiprocessing as mp
import os
from enum import Enum

import docker

import pandas as pd

import xarray as xr


DEFAULT_IMAGE = 'cmriprocan/openms-toffee'
DEFAULT_TAG = '0.13.12.dev'


def main():
    parser = argparse.ArgumentParser(
        description='Run the pipeline for the specified input group',
    )
    parser.add_argument(
        'input_dir',
        type=str,
        help='The name of the input files (e.g. mzML/a, mzML/b, ..., tof/a, tof/e)',
    )
    parser.add_argument(
        'experiment',
        type=str,
        help='The name of the experiment (e.g. sgs, roest2016, procan90)',
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='Switch on debugging',
    )
    parser.add_argument(
        '--max_dilution',
        type=int,
        default=512,
        help='Set the maximum dilution to be included. Useful for for debugging SGS (if == 512, run all files)',
    )
    args = parser.parse_args()

    allowed_input_dir = ['mzML/' + a for a in 'a,b,c,d,e,f,g'.split(',')] + ['tof/a', 'tof/e']
    if args.input_dir not in allowed_input_dir:
        raise ValueError(f'Invalid input_dir ({args.input_dir}): should be in {allowed_input_dir}')

    # get metadata
    if args.experiment == 'sgs':
        exp_key = 'sgs'
        pipeline_func = setup_sgs_pipeline
    elif args.experiment == 'roest2016':
        exp_key = 'roest2016'
        pipeline_func = setup_roest2016_pipeline
    elif args.experiment == 'procan90':
        exp_key = 'procan90'
        pipeline_func = setup_procan90_pipeline
    else:
        raise ValueError(f'Invalid experiment: {args.experiment}')

    # set up metadata
    base_dir = f'{os.getcwd()}/{exp_key}'
    metadata, srl_fname, irt_fname = pipeline_func(args, base_dir)
    results_path = f'{base_dir}/results/{args.input_dir}'

    ext, run_key = args.input_dir.split('/')
    if ext == 'tof':
        pipeline_factory = DevPipeline.default_open_ms_toffee_batch_pipeline
    else:
        pipeline_factory = DevPipeline.default_openswath_batch_pipeline
    colname_fmt_str = f'{base_dir}/{args.input_dir}/{{}}.{run_key}.{ext}'
    metadata[DevPipeline.INPUT_COLNAME] = metadata.InjectionName.apply(colname_fmt_str.format)
    metadata.set_index(DevPipeline.INDEX_NAME, inplace=True, drop=True)

    # set up pipeline
    swath_scores_kwargs = {
        'debugging': args.debug,
        'irt_srl_path': irt_fname,
    }
    fdr_kwargs = {
        'debugging': args.debug,
    }
    alignment_kwargs = {
        'debugging': args.debug,
    }
    pipeline = pipeline_factory(
        srl_path=srl_fname,
        swath_scores_kwargs=swath_scores_kwargs,
        fdr_kwargs=fdr_kwargs,
        alignment_kwargs=alignment_kwargs,
    )

    # run pipeline
    result_data_keys = [
        PyProphetColumns.INTENSITY,
        PyProphetColumns.RETENTION_TIME,
        PyProphetColumns.M_SCORE,
    ]
    ds = pipeline.run(
        metadata_df=metadata,
        results_path=results_path,
        swath_scores_kwargs={
            'use_ms1': True,
            'minimal_quality': -5,
        },
        fdr_kwargs={
            'apply_protein_fdr': False,  # doesn't make sense for this data
            'pi0_lambda': (0.0, 0.0001, 0.00001),
            'export_fdr': 0.01,
            'parametric': True,
        },
        result_data_keys=result_data_keys,
    )
    input_dir = args.input_dir.replace('/', '_')
    ds.to_netcdf(f'{results_path}/pqm_matrix-{input_dir}.nc')


def setup_sgs_pipeline(args, base_dir):
    # load metadata
    metadata = pd.read_csv(f'{base_dir}/sgs-metadata.tsv', sep='\t')
    mask1 = metadata.Background == 'Human'
    mask2 = metadata.TechnicalReplicate == 1
    mask3 = metadata.Dilution <= args.max_dilution
    metadata = metadata[mask1 & mask2 & mask3]
    metadata = metadata.sort_values('Dilution', ascending=False).reset_index()

    # set up run parameters
    srl_fname = f'{base_dir}/srl/OpenSWATH_SM4_GoldStandardAssayLibrary.pqp'
    if 'tof' in args.input_dir:
        irt_fname = f'{base_dir}/srl/OpenSWATH_SM4_iRT_AssayLibrary.tsv'
    else:
        irt_fname = f'{base_dir}/srl/OpenSWATH_SM4_iRT_AssayLibrary.TraML'

    return metadata, srl_fname, irt_fname


def setup_roest2016_pipeline(args, base_dir):
    # load metadata
    metadata = pd.read_csv(f'{base_dir}/roest2016-metadata.tsv', sep='\t')
    metadata = metadata.sort_values('InjectionName', ascending=False).reset_index()

    # set up run parameters
    srl_fname = f'{base_dir}/srl/SpyogenesAssayLibrary.with_decoys.pqp'
    if 'tof' in args.input_dir:
        irt_fname = f'{base_dir}/srl/strep_iRT_small.tsv'
    else:
        irt_fname = f'{base_dir}/srl/strep_iRT_small.TraML'

    return metadata, srl_fname, irt_fname


def setup_procan90_pipeline(args, base_dir):
    # load metadata
    metadata = pd.DataFrame([{'InjectionName': 'ProCan90-M0{}-01'.format(m), 'Machine': m} for m in range(1, 7)])

    # set up run parameters
    srl_fname = f'{base_dir}/srl/hek_srl.OpenSwath.reverse_decoys.pqp'
    if 'tof' in args.input_dir:
        irt_fname = f'{base_dir}/srl/hek_srl.OpenSwath.iRT.tsv'
    else:
        irt_fname = f'{base_dir}/srl/hek_srl.OpenSwath.iRT.TraML'

    return metadata, srl_fname, irt_fname


class PyProphetColumns(Enum):
    """
    Enumeration of interesting columns in the output of a PyPropet file
    """
    INTENSITY = 'Intensity'
    M_SCORE = 'm_score'
    D_SCORE = 'd_score'
    RETENTION_TIME = 'RT'
    ASSAY_RETENTION_TIME = 'assay_rt'
    LEFT_WIDTH = 'leftWidth'
    RIGHT_WIDTH = 'rightWidth'
    POTENTIAL_OUTLIER = 'potentialOutlier'
    PEAK_GROUP = 'peak_group_rank'
    DECOY = 'decoy'


class _PipelineStep():
    def run(self, results_path, *args, **kwargs):
        raise NotImplementedError('Called from base class')

    @classmethod
    def _makedirs(cls, path):
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise

    @classmethod
    def _extract_path(cls, p):
        p_dir, p_basename = None, None
        if p is not None:
            p_dir = os.path.abspath(os.path.dirname(p))
            p_basename = os.path.basename(p)
        return p_dir, p_basename

    @classmethod
    def _docker_run(cls, docker_client, logger, image, command, volumes):
        logger.debug('Starting %s with %s', command, image)

        container = docker_client.containers.run(
            image=image,
            command=command,
            volumes=volumes,
            detach=True,
        )
        exit_status_dict = container.wait()
        exit_status = exit_status_dict['StatusCode']
        output = container.logs(
            stdout=True,
            stderr=True,
            stream=False,
        ).decode('utf-8')
        container.remove(force=True)

        if exit_status != 0:
            raise docker.errors.ContainerError(
                container,
                exit_status,
                command,
                image,
                output,
            )
        else:
            logger.debug(output)


class _SwathScoresStep(_PipelineStep):
    """
    A class to be used for debugging with the OpenMSToffeeWorkflow pipeline
    when the full resources of the cluster or cloud are not required.

    ** Note, this should not be used for production runs **
    """

    def __init__(
        self,
        srl_path,
        irt_srl_path,
        rt_transform_path,
        docker_image,
        docker_image_tag,
        specimen_suffix,
        debugging,
    ):
        self.logger = set_stream_logger(
            name=__name__,
            level=logging.DEBUG if debugging else logging.INFO,
        )

        self.srl_dir, self.srl_basename = self._extract_path(srl_path)
        self.irt_dir, self.irt_basename = self._extract_path(irt_srl_path)
        self.rt_transform_dir, self.rt_transform_basename = self._extract_path(rt_transform_path)
        assert not (self.irt_basename is None and self.rt_transform_basename is None)

        if specimen_suffix is not None:
            assert os.path.sep not in specimen_suffix
        self.specimen_suffix = specimen_suffix

        self.is_sqlite = os.path.splitext(srl_path)[-1] == '.pqp'

        # select the docker images
        self.docker_client = docker.from_env()
        self.docker_image = f'{docker_image}:{docker_image_tag}'

    def _run_impl(
        self,
        results_path,
        input_file_fullpath,
        cmd,
        out_flag,
        use_ms1=True,
        minimal_quality=None,
        cache_dir=None,
        **kwargs,
    ):
        assert isinstance(cmd, list)

        self._makedirs(results_path)

        infile_dir, infile_basename = self._extract_path(input_file_fullpath)
        assert infile_basename is not None

        ext = '.osw' if self.is_sqlite else '.tsv'
        output_base = os.path.splitext(infile_basename)[0]
        if self.specimen_suffix is not None:
            assert os.path.sep not in self.specimen_suffix
            output_base += self.specimen_suffix
        output_basename = output_base + ext

        volumes = {}

        def add_volume(d, docker_d):
            if d not in volumes:
                volumes[d] = {'bind': docker_d, 'mode': 'rw'}
            return volumes[d]['bind']

        output_docker_dir = add_volume(results_path, '/output')
        infile_docker_dir = add_volume(infile_dir, '/input')
        srl_docker_dir = add_volume(self.srl_dir, '/input1')
        irt_docker_dir = add_volume(self.irt_dir, '/input2')
        rt_transform_docker_dir = add_volume(self.rt_transform_dir, '/input3')

        cmd.extend(('-in', f'{infile_docker_dir}/{infile_basename}'))
        cmd.extend(('-tr', f'{srl_docker_dir}/{self.srl_basename}'))
        cmd.extend((out_flag, f'{output_docker_dir}/{output_basename}'))

        if self.rt_transform_basename is None:
            cmd.extend(('-tr_irt', f'{irt_docker_dir}/{self.irt_basename}'))
            rt_transform_basename = output_base + '.trafoXML'
            cmd.extend(('-Debugging:irt_trafo', f'{output_docker_dir}/{rt_transform_basename}'))
        else:
            assert os.path.isfile(f'{self.rt_transform_dir}/{self.rt_transform_basename}')
            cmd.extend(('-rt_norm', f'{rt_transform_docker_dir}/{self.rt_transform_basename}'))

        if use_ms1:
            cmd.append('-use_ms1_traces')

        if minimal_quality is not None:
            cmd.extend(('-Scoring:TransitionGroupPicker:minimal_quality', str(minimal_quality)))

        cmd = ' '.join(cmd)
        # clean up the cache files if needed
        if cache_dir is not None:
            cmd = f'/bin/bash -c \'{cmd}; rm -f {cache_dir}*\''

        output_path = f'{results_path}/{output_basename}'
        if not os.path.isfile(output_path):
            self._docker_run(
                docker_client=self.docker_client,
                logger=self.logger,
                image=self.docker_image,
                command=cmd,
                volumes=volumes,
            )
        else:
            self.logger.debug('%s already exists, not recreating', output_path)

        return output_path


class OpenMSToffeeWorkflowStep(_SwathScoresStep):
    """
    A class to be used for debugging with the OpenMSToffeeWorkflow pipeline
    when the full resources of the cluster or cloud are not required.

    ** Note, this should not be used for production runs **
    """

    def __init__(
        self,
        srl_path,
        irt_srl_path=None,
        rt_transform_path=None,
        docker_image=DEFAULT_IMAGE,
        docker_image_tag=DEFAULT_TAG,
        specimen_suffix=None,
        debugging=False,
        **kwargs,
    ):
        """
        :param str srl_path: full path to the spectral library file in either TSV or PQP formats.
            If PQP is used, the workflow will automatically operate use SQLite for output
        :param str irt_srl_path: (optional) full path to the retention time calibration library. If
            this is not specified, the alternative rt_transform_path will be used. If neither, or both,
            are specified, an exception will be thrown
        :param str rt_transform_path: (optional) full path to a previously calculated RT normalisation
            trafoXML file. If this is not specified, the alternative irt_srl_path will be used. If
            neither, or both, are specified, an exception will be thrown
        :param str docker_image: The docker image to be used to run the workflow
        :param str docker_image_tag: The version docker image to be used to run the workflow
        :param str specimen_suffix: a suffix to append to the specimen name for output files.
            Do not include a slash.
        :param bool debugging: set to True for more verbose messaging
        """
        super().__init__(
            srl_path=srl_path,
            irt_srl_path=irt_srl_path,
            rt_transform_path=rt_transform_path,
            docker_image=docker_image,
            docker_image_tag=docker_image_tag,
            specimen_suffix=specimen_suffix,
            debugging=debugging,
        )

    def run(
        self,
        results_path,
        input_file_fullpath,
        match_openswath_defaults=True,
        use_ms1=True,
        minimal_quality=None,
        **kwargs,
    ):
        """
        Run the scoring algorithm. If the output file already exists, then we exit without
        doing anything

        :param str results_path: full path to the directory where output files will be written
        :param str input_file_fullpath: full path to the input file
        :param bool use_ms1: if true, MS1 scoring will be performed
        :param float minimal_quality: if specified, this will be passed to OpenMSToffeeWorkflow
            Scoring:TransitionGroupPicker:minimal_quality parameter

        :return str: name of the output file that was greated
        """
        assert os.path.splitext(input_file_fullpath)[1] == '.tof'

        cmd = []
        cmd.append('OpenMSToffeeWorkflow')
        if match_openswath_defaults:
            cmd.extend(('-mz_extraction_window', '0.05'))
            cmd.extend(('-mass_unit', 'thomson'))
            cmd.extend(('-irt_mz_extraction_window', '0.05'))
            cmd.extend(('-irt_mass_unit', 'thomson'))
            cmd.extend(('-rt_extraction_window', '600'))

        return self._run_impl(
            results_path=results_path,
            input_file_fullpath=input_file_fullpath,
            cmd=cmd,
            out_flag='-out',
            use_ms1=use_ms1,
            minimal_quality=minimal_quality,
            **kwargs,
        )


class OpenSwathWorkflowStep(_SwathScoresStep):
    """
    A class to be used for debugging with the OpenSwathWorkflow pipeline
    when the full resources of the cluster or cloud are not required.

    ** Note, this should not be used for production runs **
    """

    def __init__(
        self,
        srl_path,
        irt_srl_path=None,
        rt_transform_path=None,
        docker_image=DEFAULT_IMAGE,
        docker_image_tag=DEFAULT_TAG,
        specimen_suffix=None,
        debugging=False,
        **kwargs,
    ):
        """
        :param str srl_path: full path to the spectral library file in either TSV or PQP formats.
            If PQP is used, the workflow will automatically operate use SQLite for output
        :param str irt_srl_path: (optional) full path to the retention time calibration library. If
            this is not specified, the alternative rt_transform_path will be used. If neither, or both,
            are specified, an exception will be thrown
        :param str rt_transform_path: (optional) full path to a previously calculated RT normalisation
            trafoXML file. If this is not specified, the alternative irt_srl_path will be used. If
            neither, or both, are specified, an exception will be thrown
        :param str docker_image: The docker image to be used to run the workflow
        :param str docker_image_tag: The version docker image to be used to run the workflow
        :param specimen_suffix: a suffix to append to the specimen name for output files.
            Do not include a slash.
        :param debugging: set to True for more verbose messaging
        """
        super().__init__(
            srl_path=srl_path,
            irt_srl_path=irt_srl_path,
            rt_transform_path=rt_transform_path,
            docker_image=docker_image,
            docker_image_tag=docker_image_tag,
            specimen_suffix=specimen_suffix,
            debugging=debugging,
        )

    def run(
        self,
        results_path,
        input_file_fullpath,
        use_ms1=True,
        minimal_quality=None,
        **kwargs,
    ):
        """
        Run the scoring algorithm. If the output file already exists, then we exit without
        doing anything

        :param str results_path: full path to the directory where output files will be written
        :param str input_file_fullpath: full path to the input file
        :param bool use_ms1: if true, MS1 scoring will be performed
        :param float minimal_quality: if specified, this will be passed to OpenMSToffeeWorkflow
            Scoring:TransitionGroupPicker:minimal_quality parameter

        :return str: name of the output file that was greated
        """
        assert os.path.splitext(input_file_fullpath)[1] in ['.mzML', '.mzXML']

        cache_dir = '/output/cache-'
        cmd = []
        cmd.append('OpenSwathWorkflow')
        cmd.append('-sort_swath_maps')
        cmd.extend(('-threads', str(mp.cpu_count())))
        cmd.extend(('-min_upper_edge_dist', '1'))
        cmd.extend(('-readOptions', 'cache'))
        cmd.extend(('-tempDirectory', cache_dir))
        cmd.append('-force')

        return self._run_impl(
            results_path=results_path,
            input_file_fullpath=input_file_fullpath,
            cmd=cmd,
            out_flag='-out_osw' if self.is_sqlite else '-out_tsv',
            use_ms1=use_ms1,
            minimal_quality=minimal_quality,
            cache_dir=cache_dir,
            **kwargs,
        )


class _PyProphetStep(_PipelineStep):
    """
    A class to be used for debugging a batch oriented pipeline when the full
    resources of the cluster or cloud are not required.

    ** Note, this should not be used for production runs **
    """

    def __init__(
        self,
        docker_image,
        docker_image_tag,
        debugging,
        **kwargs,
    ):
        """
        :param str docker_image: The docker image to be used to run the workflow
        :param str docker_image_tag: The version docker image to be used to run the workflow
        :param debugging: set to True for more verbose messaging
        """
        self.logger = set_stream_logger(
            name=__name__,
            level=logging.DEBUG if debugging else logging.INFO,
        )

        # select the docker images
        self.docker_client = docker.from_env()
        self.docker_image = f'{docker_image}:{docker_image_tag}'

    def run(
        self,
        results_path,
        score_files,
        run_name=None,
        **kwargs,
    ):
        """
        Create and run the PyProphet script. If the script has not changed (as judged by
        calculating a SHA256 hash) then we don't re-generate the results

        :param str results_path: full path to the directory where output files will be written
        :param list(str) score_files: a list of file paths that were generated from the swath
            scoring step. For this batch workflow, these must be from the SQLite workflow.
        :param dict kwargs: a set of key-value pairs that are passed into the function that
            creates the pyprophet script. See _build_pyprophet_script for more details.
        :param str run_name: a string that is used to describe the current run

        :return list(str): a list of paths to the output TSV files -- the order of the list
            corresponds to the order of the score_files
        """
        assert run_name is not None, 'You must supply run_name to describe the current run'
        self._makedirs(results_path)

        pyprophet_script_basename = f'run_pyprophet.{run_name}.sh'
        self._build_pyprophet_script(run_name, results_path, score_files, pyprophet_script_basename, **kwargs)
        self._run_pyprophet(results_path, pyprophet_script_basename)

        fdr_files = [os.path.splitext(fname)[0] + f'.pyprophet.{run_name}.tsv' for fname in score_files]
        return fdr_files

    def _build_pyprophet_script(
        self,
        run_name,
        results_path,
        score_files,
        pyprophet_script_basename,
        parametric=False,
        pi0_lambda=None,
        apply_protein_fdr=True,
        export_fdr=None,
    ):
        """
        :param str run_name: a string that is used to describe the current run
        :param str results_path: full path to the directory where output files will be written
        :param list(str) score_files: a list of file paths that were generated from the swath
            scoring step. For this batch workflow, these must be from the SQLite workflow.
        :param str pyprophet_script_basename: name of the file to save the script in to
        :param bool parametric: if True, a parametric model is used in pyprophet
        :param tuple(3) pi0_lambda: (optional) if speficied, this is a tuple of floats that is
            used to control the pi0 lambda parameter of pyprophet
        :param bool apply_protein_fdr: if false, protein FDR calculations are excluded
        """
        raise NotImplementedError('Called from base class')

    def _run_pyprophet(self, results_path, pyprophet_script_basename):
        """
        :param str results_path: full path to the directory where output files will be written
        :param str pyprophet_script_basename: name of the file to save the script in to
        """
        working_dir = '/output'

        volumes = {}
        volumes[results_path] = {'bind': working_dir, 'mode': 'rw'}

        cmd = f'/bin/bash -c \'cd {working_dir}; /bin/bash {pyprophet_script_basename}\''

        # use a hash of the run script to see if it has been run before
        pyprophet_script_path = f'{results_path}/{pyprophet_script_basename}'
        pyprophet_script_path_sha256 = f'{pyprophet_script_path}.sha256'
        with open(pyprophet_script_path, 'rb') as f:
            current_sha256 = hashlib.sha256(f.read()).hexdigest()
        new_sha256 = ''
        if os.path.isfile(pyprophet_script_path_sha256):
            with open(pyprophet_script_path_sha256, 'r') as f:
                new_sha256 = f.read()

        if current_sha256 != new_sha256:
            self._docker_run(
                docker_client=self.docker_client,
                logger=self.logger,
                image=self.docker_image,
                command=cmd,
                volumes=volumes,
            )
            with open(pyprophet_script_path_sha256, 'w') as f:
                f.write(current_sha256)
        else:
            self.logger.debug(f'SHA256 of {pyprophet_script_path} unchanged')


class PyProphetBatchStep(_PyProphetStep):
    """
    A class to be used for debugging a batch oriented pipeline when the full
    resources of the cluster or cloud are not required.

    ** Note, this should not be used for production runs **
    """

    def __init__(
        self,
        docker_image=DEFAULT_IMAGE,
        docker_image_tag=DEFAULT_TAG,
        debugging=False,
        **kwargs,
    ):
        """
        :param str docker_image: The docker image to be used to run the workflow
        :param str docker_image_tag: The version docker image to be used to run the workflow
        :param debugging: set to True for more verbose messaging
        """
        super().__init__(
            docker_image=docker_image,
            docker_image_tag=docker_image_tag,
            debugging=debugging,
            **kwargs,
        )

    def _build_pyprophet_script(
        self,
        run_name,
        results_path,
        score_files,
        pyprophet_script_basename,
        sub_sample_scale=1,
        parametric=False,
        pi0_lambda=None,
        export_fdr=None,
        apply_peptide_fdr=True,
        apply_protein_fdr=True,
    ):
        """
        See super()._build_pyprophet_script()
        """
        self.logger.debug(f'Building {pyprophet_script_basename}')

        extensions = list(set([os.path.splitext(f)[1].lower() for f in score_files]))
        assert len(extensions) == 1
        ext = extensions[0]
        assert ext == '.osw', f'Batch mode only works with *.osw files, not {ext}'

        dirs = list(set([os.path.dirname(f) for f in score_files]))
        assert len(dirs) == 1, f'Script assumes all files are in the same directory, not {dirs}'

        if pi0_lambda is not None:
            assert isinstance(pi0_lambda, tuple)
            assert len(pi0_lambda) == 3

        max_num_files = 10000
        assert sub_sample_scale <= len(score_files) < max_num_files
        # round the sub-sampling ratio to match the maximum number of files
        subsample_ratio = (sub_sample_scale * max_num_files // len(score_files)) / max_num_files
        assert 0.0 < subsample_ratio <= sub_sample_scale
        model_scoring_fname = f'model_scoring_{run_name}.osw'
        model_fdr_fname = f'model_fdr_{run_name}.osw'

        script = []
        script.append('#!/bin/bash')
        script.append('set -e')
        script.append('echo `pwd`')

        # set up files of interest
        script.append('')
        script.append('INPUT_FILES=""')
        template_fname = None  # use this later when merging sub-sampled files
        for fname in score_files:
            fname = os.path.basename(fname)
            script.append(f'INPUT_FILES+=" {fname}"')
            if template_fname is None:
                template_fname = fname

        # ---
        # STEP 1: subsample the files
        script.append('')
        script.append('echo "' + '*' * 80 + '"')
        script.append('echo "Step 1: subsample files"')
        script.append(f'rm -f {model_scoring_fname}')
        script.append('')
        script.append('SUBSAMPLED_INPUT_FILES=""')
        script.append('for in_fname in ${INPUT_FILES}; do')
        script.append('\tsubsampled_fname=${in_fname}s  # generates *.osws files')
        script.append('\trm -f $subsampled_fname')
        cmd = '\tpyprophet subsample'
        cmd += ' \\\n\t\t--in=$in_fname'
        cmd += ' \\\n\t\t--out=$subsampled_fname'
        cmd += f' \\\n\t\t--subsample_ratio={subsample_ratio}'
        script.append(cmd)
        script.append('\tSUBSAMPLED_INPUT_FILES+=" $subsampled_fname"')
        script.append('done')
        script.append('')

        # ---
        # STEP 2: merge and score
        script.append('')
        script.append('echo "' + '*' * 80 + '"')
        script.append('echo "Step 2: merge subsampled files and then score"')
        script.append('')
        script.append('# --- Step 2.A ' + '-' * 20)
        cmd = 'pyprophet merge'
        cmd += f' \\\n\t--out={model_scoring_fname}'
        cmd += f' \\\n\t--template={template_fname}'
        cmd += ' \\\n\t${SUBSAMPLED_INPUT_FILES}'
        script.append(cmd)
        script.append('rm -f ${SUBSAMPLED_INPUT_FILES}')

        # score the merged, sub-sampled file
        script.append('')
        script.append('# --- Step 2.B ' + '-' * 20)
        cmd = 'pyprophet score'
        cmd += f' \\\n\t--in={model_scoring_fname}'
        cmd += ' \\\n\t--level=ms1ms2'
        if parametric:
            cmd += ' \\\n\t--parametric'
        if pi0_lambda is not None:
            cmd += ' \\\n\t--pi0_lambda {} {} {}'.format(*pi0_lambda)
        script.append(cmd)

        # ---
        # STEP 3: Apply the model
        script.append('')
        script.append('echo "' + '*' * 80 + '"')
        script.append('echo "Step 3: apply the model and extract data to reduce size for FDR control"')
        script.append('REDUCED_INPUT_FILES=""')
        script.append('for in_fname in ${INPUT_FILES}; do')

        script.append('')
        script.append('\t# --- Step 3.A ' + '-' * 20)
        cmd = '\tpyprophet score'
        cmd += ' \\\n\t\t--in=$in_fname'
        cmd += ' \\\n\t\t--level=ms1ms2'
        cmd += f' \\\n\t\t--apply_weights={model_scoring_fname}'
        if parametric:
            cmd += ' \\\n\t\t--parametric'
        if pi0_lambda is not None:
            cmd += ' \\\n\t\t--pi0_lambda {} {} {}'.format(*pi0_lambda)
        script.append(cmd)

        script.append('')
        script.append('\t# --- Step 3.B ' + '-' * 20)
        script.append('\treduced_fname=${in_fname}r  # generates *.oswr files')
        script.append('\trm -f $reduced_fname')
        cmd = '\tpyprophet reduce'
        cmd += ' \\\n\t\t--in=$in_fname'
        cmd += ' \\\n\t\t--out=$reduced_fname'
        script.append(cmd)
        script.append('\tREDUCED_INPUT_FILES+=" $reduced_fname"')

        script.append('done')

        # ---
        # STEP 4: merge scored files and calculate global FDR
        script.append('')
        script.append('echo "' + '*' * 80 + '"')
        script.append('echo "Step 4: merge scored files and calculate global FDR"')

        script.append('')
        script.append('# --- Step 4.A ' + '-' * 20)
        script.append(f'rm -f {model_fdr_fname}')
        cmd = 'pyprophet merge'
        cmd += f' \\\n\t--template={model_scoring_fname}'
        cmd += f' \\\n\t--out={model_fdr_fname}'
        cmd += ' \\\n\t${REDUCED_INPUT_FILES}'
        script.append(cmd)
        script.append('rm -f ${REDUCED_INPUT_FILES}')

        # calculate FDR model for peptide and protein, for all contexts
        script.append('')
        script.append('# --- Step 4.B ' + '-' * 20)
        fdr_levels = ['peptide']
        if apply_protein_fdr:
            fdr_levels.append('protein')
        context = 'global'
        for fdr_level in fdr_levels:
            script.append('')
            script.append(f'echo "{fdr_level} :: {context}"')
            cmd = f'pyprophet {fdr_level}'
            cmd += f' \\\n\t--in={model_fdr_fname}'
            cmd += f' \\\n\t--context={context}'
            if parametric:
                cmd += ' \\\n\t--parametric'
            if pi0_lambda is not None:
                cmd += ' \\\n\t--pi0_lambda {} {} {}'.format(*pi0_lambda)
            script.append(cmd)

        # ---
        # STEP 5: backpropagate results into full files
        script.append('')
        script.append('echo "' + '*' * 80 + '"')
        script.append('echo "Step 5: backpropagate results and export to TSV"')
        script.append('for in_fname in ${INPUT_FILES}; do')
        script.append('\tfdr_fname="fdr_${in_fname}"')
        script.append(f'\ttsv_fname="${{in_fname%.*}}.pyprophet.{run_name}.tsv"')

        script.append('')
        script.append('\t# --- Step 5.A ' + '-' * 20)
        script.append('\techo "backpropagating results of $in_fname to $tsv_fname"')
        cmd = '\tpyprophet backpropagate'
        cmd += ' \\\n\t\t--in=$in_fname'
        cmd += ' \\\n\t\t--out=$fdr_fname'
        cmd += f' \\\n\t\t--apply_scores={model_fdr_fname}'
        script.append(cmd)

        script.append('')
        script.append('\t# --- Step 5.B ' + '-' * 20)
        cmd = '\tpyprophet export'
        cmd += ' \\\n\t\t--in=$fdr_fname'
        cmd += ' \\\n\t\t--out=$tsv_fname'
        cmd += ' \\\n\t\t--format=legacy_merged'
        if not apply_protein_fdr:
            cmd += ' \\\n\t\t--no-protein'
        if export_fdr is not None:
            cmd += f' \\\n\t\t--max_rs_peakgroup_qvalue={export_fdr}'
            cmd += f' \\\n\t\t--max_global_protein_qvalue={export_fdr}'
            cmd += f' \\\n\t\t--max_global_peptide_qvalue={export_fdr}'
        script.append(cmd)
        script.append('\trm -f $fdr_fname')

        script.append('done')

        output_path = f'{results_path}/{pyprophet_script_basename}'
        with open(output_path, 'w') as f:
            f.write('\n'.join(script))


class PyProphetIndividualStep(_PyProphetStep):
    """
    A class to be used for debugging a batch oriented pipeline when the full
    resources of the cluster or cloud are not required.

    ** Note, this should not be used for production runs **
    """

    def __init__(
        self,
        docker_image=DEFAULT_IMAGE,
        docker_image_tag=DEFAULT_TAG,
        debugging=False,
        **kwargs,
    ):
        """
        :param str docker_image: The docker image to be used to run the workflow
        :param str docker_image_tag: The version docker image to be used to run the workflow
        :param debugging: set to True for more verbose messaging
        """
        super().__init__(
            docker_image=docker_image,
            docker_image_tag=docker_image_tag,
            debugging=debugging,
            **kwargs,
        )

    def _build_pyprophet_script(
        self,
        run_name,
        results_path,
        score_files,
        pyprophet_script_basename,
        parametric=False,
        pi0_lambda=None,
        export_fdr=None,
        apply_peptide_fdr=True,
        apply_protein_fdr=True,
    ):
        """
        See super()._build_pyprophet_script()
        """
        self.logger.debug(f'Building {pyprophet_script_basename}')

        extensions = list(set([os.path.splitext(f)[1].lower() for f in score_files]))
        assert len(extensions) == 1
        ext = extensions[0]
        assert ext == '.osw', f'Batch mode only works with *.osw files, not {ext}'

        dirs = list(set([os.path.dirname(f) for f in score_files]))
        assert len(dirs) == 1, f'Script assumes all files are in the same directory, not {dirs}'

        if pi0_lambda is not None:
            assert isinstance(pi0_lambda, tuple)
            assert len(pi0_lambda) == 3

        script = []
        script.append('#!/bin/bash')
        script.append('set -e')
        script.append('echo `pwd`')

        # set up files of interest
        script.append('')
        script.append('INPUT_FILES=""')
        template_fname = None  # use this later when merging sub-sampled files
        for fname in score_files:
            fname = os.path.basename(fname)
            script.append(f'INPUT_FILES+=" {fname}"')
            if template_fname is None:
                template_fname = fname

        script.append('')
        script.append('for in_fname in ${INPUT_FILES}; do')
        script.append('')
        script.append('\techo "' + '*' * 80 + '"')
        script.append('\techo "Working on $in_fname"')

        # ---
        # STEP 1: score the model
        script.append('')
        script.append('\techo "' + '*' * 80 + '"')
        script.append('\techo "Step 1: score model"')
        cmd = '\tpyprophet score'
        cmd += ' \\\n\t\t--in=$in_fname'
        cmd += ' \\\n\t\t--level=ms1ms2'
        if parametric:
            cmd += ' \\\n\t\t--parametric'
        if pi0_lambda is not None:
            cmd += ' \\\n\t\t--pi0_lambda {} {} {}'.format(*pi0_lambda)
        script.append(cmd)

        # ---
        # STEP 2: calculate FDR model
        script.append('')
        script.append('\techo "' + '*' * 80 + '"')
        script.append('\techo "Step 2: calculate FDR controls"')
        context = 'global'  # all three contexts are equivalent here
        fdr_levels = ['peptide']
        if apply_protein_fdr:
            fdr_levels.append('protein')
        for fdr_level in fdr_levels:
            script.append(f'echo "{fdr_level} :: {context}"')
            cmd = f'\tpyprophet {fdr_level}'
            cmd += ' \\\n\t\t--in=$in_fname'
            cmd += f' \\\n\t\t--context={context}'
            if parametric:
                cmd += ' \\\n\t\t--parametric'
            if pi0_lambda is not None:
                cmd += ' \\\n\t\t--pi0_lambda {} {} {}'.format(*pi0_lambda)
            script.append('')
            script.append(cmd)

        # ---
        # STEP 3: export results into tsv file
        script.append('')
        script.append('\techo "' + '*' * 80 + '"')
        script.append(f'\ttsv_fname="${{in_fname%.*}}.pyprophet.{run_name}.tsv"')
        script.append('\techo "Step 3: export to TSV: $in_fname --> $tsv_fname"')
        cmd = '\tpyprophet export'
        cmd += ' \\\n\t\t--in=$in_fname'
        cmd += ' \\\n\t\t--out=$tsv_fname'
        cmd += ' \\\n\t\t--format=legacy_merged'
        if not apply_protein_fdr:
            cmd += ' \\\n\t\t--no-protein'
        if export_fdr is not None:
            cmd += f' \\\n\t\t--max_rs_peakgroup_qvalue={export_fdr}'
            cmd += f' \\\n\t\t--max_global_protein_qvalue={export_fdr}'
            cmd += f' \\\n\t\t--max_global_peptide_qvalue={export_fdr}'
        script.append(cmd)

        script.append('')
        script.append('done')

        output_path = f'{results_path}/{pyprophet_script_basename}'
        with open(output_path, 'w') as f:
            f.write('\n'.join(script))


class NoOpAlignmentStep(_PipelineStep):
    """A No Operation class for the alignment step"""
    def __init__(self, **kwargs):
        pass

    def run(self, results_path, fdr_files, *args, **kwargs):
        return fdr_files


class TricAlignmentStep(_PipelineStep):
    """
    A class to be used for debugging a the TRIC workflow

    ** Note, this should not be used for production runs **
    """

    def __init__(
        self,
        docker_image='cmriprocan/openms-toffee',
        docker_image_tag='0.13.10.dev',
        debugging=False,
        **kwargs,
    ):
        """
        :param str docker_image: The docker image to be used to run the workflow
        :param str docker_image_tag: The version docker image to be used to run the workflow
        :param debugging: set to True for more verbose messaging
        """
        self.logger = set_stream_logger(
            name=__name__,
            level=logging.DEBUG if debugging else logging.INFO,
        )

        # select the docker images
        self.docker_client = docker.from_env()
        self.docker_image = f'{docker_image}:{docker_image_tag}'

    def run(
        self,
        results_path,
        fdr_files,
        **kwargs,
    ):
        """
        Create and run the PyProphet script. If the script has not changed (as judged by
        calculating a SHA256 hash) then we don't re-generate the results

        :param str results_path: full path to the directory where output files will be written
        :param list(str) fdr_files: a list of file paths that were generated from the pyprophet step
        :param dict kwargs: a set of key-value pairs that are passed into the function that
            creates the tric script. See _build_tric_script for more details.
        :param str run_name: a string that is used to describe the current run

        :return list(str): a list of paths to the output TSV files -- the order of the list
            corresponds to the order of the score_files
        """
        self._makedirs(results_path)

        tric_script_basename = f'run_tric.sh'
        alignment_fname = f'tric_alignment.tsv'
        self._build_tric_script(
            results_path,
            fdr_files,
            tric_script_basename,
            alignment_fname,
            **kwargs,
        )
        self._run_tric(results_path, tric_script_basename)
        return f'{results_path}/{alignment_fname}'

    def _build_tric_script(
        self,
        results_path,
        fdr_files,
        tric_script_basename,
        alignment_fname,
        fdr_cutoff=None,
        **kwargs,
    ):
        """
        :param str results_path: full path to the directory where output files will be written
        :param list(str) fdr_files: a list of file paths that were generated from the pyprophet step
        :param str tric_script_basename: name of the file to save the script in to
        :param str alignment_fname: name of the resulting TRIC file
        """

        extensions = list(set([os.path.splitext(f)[1].lower() for f in fdr_files]))
        assert len(extensions) == 1
        ext = extensions[0]
        assert ext == '.tsv', f'Batch mode only works with *.tsv files, not {ext}'

        dirs = list(set([os.path.dirname(f) for f in fdr_files]))
        assert len(dirs) == 1, f'Script assumes all files are in the same directory, not {dirs}'

        script = []
        script.append('#!/bin/bash')
        script.append('set -e')
        script.append('echo `pwd`')

        # set up files of interest
        script.append('')
        script.append('INPUT_FILES=""')
        for fname in fdr_files:
            if pd.read_csv(fname, sep='\t').shape[0] == 0:
                continue
            fname = os.path.basename(fname)
            script.append(f'INPUT_FILES+=" {fname}"')

        # ---
        # STEP 1: run TRIC
        script.append('')
        script.append('echo "' + '*' * 80 + '"')
        script.append('echo "Step 1: run TRIC"')
        cmd = 'feature_alignment.py'
        cmd += ' \\\n\t--in $INPUT_FILES'
        cmd += f' \\\n\t--out {alignment_fname}'
        cmd += ' \\\n\t--method LocalMST'
        cmd += ' \\\n\t--realign_method lowess_cython'
        cmd += ' \\\n\t--max_rt_diff 60'
        cmd += ' \\\n\t--mst:useRTCorrection True'
        cmd += ' \\\n\t--mst:Stdev_multiplier 3.0'
        cmd += ' \\\n\t--max_fdr_quality 0.05'
        if fdr_cutoff is not None:
            cmd += f' \\\n\t--fdr_cutoff {fdr_cutoff}'
        else:
            cmd += ' \\\n\t--target_fdr 0.01'
        script.append(cmd)

        output_path = f'{results_path}/{tric_script_basename}'
        with open(output_path, 'w') as f:
            f.write('\n'.join(script))

    def _run_tric(self, results_path, tric_script_basename):
        """
        :param str results_path: full path to the directory where output files will be written
        :param str tric_script_basename: name of the file to save the script in to
        """
        working_dir = '/output'

        volumes = {}
        volumes[results_path] = {'bind': working_dir, 'mode': 'rw'}

        cmd = f'/bin/bash -c \'cd {working_dir}; /bin/bash {tric_script_basename}\''

        # use a hash of the run script to see if it has been run before
        tric_script_path = f'{results_path}/{tric_script_basename}'
        tric_script_path_sha256 = f'{tric_script_path}.sha256'
        with open(tric_script_path, 'rb') as f:
            current_sha256 = hashlib.sha256(f.read()).hexdigest()
        new_sha256 = ''
        if os.path.isfile(tric_script_path_sha256):
            with open(tric_script_path_sha256, 'r') as f:
                new_sha256 = f.read()

        if current_sha256 != new_sha256:
            self._docker_run(
                docker_client=self.docker_client,
                logger=self.logger,
                image=self.docker_image,
                command=cmd,
                volumes=volumes,
            )
            with open(tric_script_path_sha256, 'w') as f:
                f.write(current_sha256)
        else:
            self.logger.debug(f'SHA256 of {tric_script_path} unchanged')


class DevPipeline():
    # The following define keys that should be included in the metadata pandas DataFrame
    INDEX_NAME = 'InjectionName'
    INPUT_COLNAME = 'InjectionFilePath'
    SCORES_COLNAME = 'ScoresFilePath'
    FDR_COLNAME = 'FdrFilePath'
    RESULT_COLNAME = 'ResultTsvFilePath'

    def __init__(
        self,
        swath_scores_step,
        fdr_step,
        alignment_step,
        debugging=False,
        **kwargs,
    ):
        """
        :param _PipelineStep swath_scores_step: The pipeline step that takes a spectral library, or equivalent,
            and calculates scores that can be put through an FDR algorithm such as PyProphet
        :param _PipelineStep fdr_step: The pipeline step that takes a list of results from the swath scores
            step and calculates summary scores that determine FDR.
        :param _PipelineStep alignment_step: The pipeline step that performs the multi-run alignment
        :param debugging: set to True for more verbose messaging
        """
        self.logger = set_stream_logger(
            name=__name__,
            level=logging.DEBUG if debugging else logging.INFO,
        )
        assert isinstance(swath_scores_step, _PipelineStep)
        assert isinstance(fdr_step, _PipelineStep)
        assert isinstance(alignment_step, _PipelineStep)
        self.swath_scores_step = swath_scores_step
        self.fdr_step = fdr_step
        self.alignment_step = alignment_step

    @classmethod
    def default_openswath_batch_pipeline(
        cls,
        srl_path,
        swath_scores_kwargs=None,
        fdr_kwargs=None,
        alignment_kwargs=None,
        **kwargs,
    ):
        """
        Create a pipeline for running OpenSwath in batch mode -- i.e. PyProphet is run with global context

        :param str srl_path: full path to the spectral library file in either TSV or PQP formats.
            If PQP is used, the workflow will automatically operate use SQLite for output
        :param dict kwargs: keyword args that are passed through to constructors of the individual steps
        """
        if swath_scores_kwargs is None:
            swath_scores_kwargs = {}
        if fdr_kwargs is None:
            fdr_kwargs = {}
        if alignment_kwargs is None:
            alignment_kwargs = {}
        swath_scores_step = OpenSwathWorkflowStep(srl_path, **swath_scores_kwargs)
        fdr_step = PyProphetBatchStep(**fdr_kwargs)
        alignment_step = NoOpAlignmentStep(**alignment_kwargs)
        # alignment_step = TricAlignmentStep(**alignment_kwargs)
        return cls(swath_scores_step, fdr_step, alignment_step, **kwargs)

    @classmethod
    def default_openswath_individual_pipeline(
        cls,
        srl_path,
        swath_scores_kwargs=None,
        fdr_kwargs=None,
        alignment_kwargs=None,
        **kwargs,
    ):
        """
        Create a pipeline for running OpenSwath in sample specific mode -- i.e. each file is run through
        PyProphet independently

        :param str srl_path: full path to the spectral library file in either TSV or PQP formats.
            If PQP is used, the workflow will automatically operate use SQLite for output
        :param dict kwargs: keyword args that are passed through to constructors of the individual steps
        """
        if swath_scores_kwargs is None:
            swath_scores_kwargs = {}
        if fdr_kwargs is None:
            fdr_kwargs = {}
        if alignment_kwargs is None:
            alignment_kwargs = {}
        swath_scores_step = OpenSwathWorkflowStep(srl_path, **swath_scores_kwargs)
        fdr_step = PyProphetIndividualStep(**fdr_kwargs)
        alignment_step = NoOpAlignmentStep(**alignment_kwargs)
        return cls(swath_scores_step, fdr_step, alignment_step, **kwargs)

    @classmethod
    def default_open_ms_toffee_batch_pipeline(
        cls,
        srl_path,
        swath_scores_kwargs=None,
        fdr_kwargs=None,
        alignment_kwargs=None,
        **kwargs,
    ):
        """
        Create a pipeline for running OpenMSToffee in batch mode -- i.e. PyProphet is run with global context

        :param str srl_path: full path to the spectral library file in either TSV or PQP formats.
            If PQP is used, the workflow will automatically operate use SQLite for output
        :param dict kwargs: keyword args that are passed through to constructors of the individual steps
        """
        if swath_scores_kwargs is None:
            swath_scores_kwargs = {}
        if fdr_kwargs is None:
            fdr_kwargs = {}
        if alignment_kwargs is None:
            alignment_kwargs = {}
        swath_scores_step = OpenMSToffeeWorkflowStep(srl_path, **swath_scores_kwargs)
        fdr_step = PyProphetBatchStep(**fdr_kwargs)
        alignment_step = NoOpAlignmentStep(**alignment_kwargs)
        # alignment_step = TricAlignmentStep(**alignment_kwargs)
        return cls(swath_scores_step, fdr_step, alignment_step, **kwargs)

    @classmethod
    def default_open_ms_toffee_individual_pipeline(
        cls,
        srl_path,
        swath_scores_kwargs=None,
        fdr_kwargs=None,
        alignment_kwargs=None,
        **kwargs,
    ):
        """
        Create a pipeline for running OpenMSToffee in batch mode -- i.e. each file is run through
        PyProphet independently

        :param str srl_path: full path to the spectral library file in either TSV or PQP formats.
            If PQP is used, the workflow will automatically operate use SQLite for output
        :param dict kwargs: keyword args that are passed through to constructors of the individual steps
        """
        if swath_scores_kwargs is None:
            swath_scores_kwargs = {}
        if fdr_kwargs is None:
            fdr_kwargs = {}
        if alignment_kwargs is None:
            alignment_kwargs = {}
        swath_scores_step = OpenMSToffeeWorkflowStep(srl_path, **swath_scores_kwargs)
        fdr_step = PyProphetIndividualStep(**fdr_kwargs)
        alignment_step = NoOpAlignmentStep(**alignment_kwargs)
        return cls(swath_scores_step, fdr_step, alignment_step, **kwargs)

    def run(
        self,
        metadata_df,
        fdr_niter=10,
        results_path='results/',
        result_data_keys=None,
        swath_scores_kwargs=None,
        fdr_kwargs=None,
        alignment_kwargs=None,
        result_extraction_kwargs=None,
    ):
        """
        Run the pipeline

        :param pandas.DataFrame metadata_df: a dataframe that defines the metadata for the study. It must contain
            the columns specified in this class -- INDEX_NAME must be the name of the index; INPUT_COLNAME must
            specify the full path to each of the input files.
        :param int fdr_niter: Number of iterations to conduct of the FDR and alignment steps. This is born out
            of an assumption that calculating the FDR has an element of randomness
        :param str results_path: full path to the directory where output files will be written
        :param list(PyProphetColumns) result_data_keys: (optional) a list of PyProphetColumns keys for which we want
            to extract the final data. If this is None, then PyProphetColumns.INTENSITY will be used
        :param dict swath_scores_kwargs: keyword arguments to be passed to the swath scoring step
        :param dict fdr_kwargs: keyword arguments to be passed to the FDR step
        :param dict alignment_kwargs: keyword arguments to be passed to the alignment step
        :param dict result_extraction_kwargs: keyword arguments to be passed to the result extraction step

        :return dict: A dictionary of results where the keys are PyProphetColumns that correspond to the
            result_data_keys and the values are AnnData objects with metadata_df set as the obs, and the PQM
            as the var
        """
        assert isinstance(metadata_df, pd.DataFrame)
        assert self.INDEX_NAME == metadata_df.index.name
        assert self.INPUT_COLNAME in metadata_df.columns

        results_path = os.path.abspath(results_path)

        if result_data_keys is None:
            result_data_keys = [PyProphetColumns.INTENSITY]
        if not isinstance(result_data_keys, (list, tuple)):
            raise ValueError(f'Invalid parameter for data_keys. It must be a list: {result_data_keys}')
        for key in result_data_keys:
            if not isinstance(key, PyProphetColumns):
                raise ValueError(f'Invalid data key ({key}) in {result_data_keys}')

        if swath_scores_kwargs is None:
            swath_scores_kwargs = {}
        if fdr_kwargs is None:
            fdr_kwargs = {}
        if alignment_kwargs is None:
            alignment_kwargs = {}
        if result_extraction_kwargs is None:
            result_extraction_kwargs = {}

        run_name = fdr_kwargs.pop('run_name', '')

        # swath scores is always embarrassingly parallel
        self.logger.info(
            'Generating DIA scores with %s',
            self.swath_scores_step.__class__.__name__,
        )
        metadata_df[self.SCORES_COLNAME] = ''
        for idx, row in metadata_df.iterrows():
            metadata_df.loc[idx, self.SCORES_COLNAME] = self.swath_scores_step.run(
                results_path,
                row[self.INPUT_COLNAME],
                **swath_scores_kwargs,
            )

        results = []
        for i in range(fdr_niter):
            df = metadata_df.copy()

            # FDR can be calcualted either as a batch, or individually
            self.logger.info(
                'Conducting FDR scoring with %s || %s of %s',
                self.fdr_step.__class__.__name__,
                i + 1,
                fdr_niter,
            )
            df[self.FDR_COLNAME] = self.fdr_step.run(
                results_path,
                df[self.SCORES_COLNAME],
                run_name=f'{run_name}{i:04d}',
                **fdr_kwargs,
            )

            # FDR can be calcualted either as a batch, or individually
            self.logger.info(
                'Doing multirun alignment with %s || %s of %s',
                self.alignment_step.__class__.__name__,
                i + 1,
                fdr_niter,
            )
            df[self.RESULT_COLNAME] = self.alignment_step.run(
                results_path,
                df[self.FDR_COLNAME],
                **alignment_kwargs,
            )

            # convert the results into AnnData objects
            self.logger.info('Extracting results || %s of %s', i + 1, fdr_niter)
            ds = self._get_results(
                metadata_df=df,
                data_keys=result_data_keys,
                fdr_iter=i,
                **result_extraction_kwargs,
            )
            results.append(ds)

        # merge the datasets
        return xr.concat(results, dim=self.INDEX_NAME)

    def _get_results(
        self,
        metadata_df,
        data_keys,
        max_m_score=-1,
        drop_decoys=True,
        filter_peak_groups=True,
        fdr_iter=0,
    ):
        """
        :param pandas.DataFrame metadata_df: a dataframe that defines the metadata for the study. It must contain
            the columns specified in this class -- INDEX_NAME must be the name of the index; INPUT_COLNAME must
            specify the full path to each of the input files.
        :param list(PyProphetColumns) result_data_keys: (optional) a list of PyProphetColumns keys for which we want
            to extract the final data. If this is None, then PyProphetColumns.INTENSITY will be used
        :param float max_m_score: sets an upper limit on the allowable `m_score` when filterin the results
        :param bool drop_decoys: if True, decoy transition groups will be dropped from the results
        :param int fdr_iter: the iteration of the FDR step that these results were generated by
        """
        colname_map = {
            'ProteinName': 'Protein',
            'Sequence': 'Peptide',
            'FullPeptideName': 'ModifiedPeptide',
            'Charge': 'Charge',
        }
        assert self.RESULT_COLNAME in metadata_df.columns

        # ---
        # load results from disk into pandas
        pandas_dict = {k: [] for k in data_keys}
        for idx, row in metadata_df.iterrows():
            df = pd.read_csv(row[self.RESULT_COLNAME], sep='\t')

            # drop decoys, m_scores, and get only the top peak groups
            if drop_decoys:
                df = df[df[PyProphetColumns.DECOY.value] == 0]
            if max_m_score >= 0:
                df = df[df[PyProphetColumns.M_SCORE.value] <= max_m_score]
            if filter_peak_groups:
                df = df[df[PyProphetColumns.PEAK_GROUP.value] == 1]
            df = df.reset_index(drop=True)
            df.rename(columns=colname_map, inplace=True)
            df[self.INDEX_NAME] = f'{idx}-{fdr_iter}'

            # with sqlite workflow, these are integers
            df['PQM'] = df.ModifiedPeptide + '_' + df.Charge.map(str)

            # extract data for each of the keys
            for key in data_keys:
                df_colnames = [key.value, 'PQM', self.INDEX_NAME] + [v for k, v in colname_map.items()]
                key_df = df[df_colnames].copy()
                pandas_dict[key].append(key_df)

        # ---
        # concat, merge with metadata and transpose
        pandas_dict_merged = {}
        first_result = None  # use the first good result to build the DataSet coords
        for key in data_keys:
            pandas_result = pandas_dict[key]

            if len(pandas_result) == 0:
                pandas_dict_merged[key] = None
                continue
            elif len(pandas_result) == 1:
                pandas_result = pandas_result[0]
            else:
                pandas_result = pd.concat(pandas_result)

            # merge results and meta data
            # pandas_result = pandas_result.merge(uid_sample_mapping, on=instance_uid_colname)

            # set up the index and pivot
            col_indexes = ['PQM'] + list(colname_map.values())
            row_indexes = [self.INDEX_NAME]
            pandas_result = pandas_result.set_index(col_indexes + row_indexes)
            pandas_result = pandas_result[key.value]
            pandas_result = pandas_result.unstack(col_indexes)

            if first_result is None:
                first_result = pandas_result

            pandas_dict_merged[key] = pandas_result

        # Note, it was considered to optionally allow the return of just pandas_dict_merged for use however
        # xarray provides many auxilliary benefits so we decided to leave it here.

        # ---
        # create xarray dataset
        if first_result is None:
            dataset = xr.Dataset()
        else:
            def idx_values(df_idx, name):
                return df_idx.get_level_values(name).tolist()

            dataset = xr.Dataset(
                coords={
                    self.INDEX_NAME: idx_values(first_result.index, self.INDEX_NAME),
                    'PQM': idx_values(first_result.columns, 'PQM'),
                    'Protein': ('PQM', idx_values(first_result.columns, 'Protein')),
                    'Peptide': ('PQM', idx_values(first_result.columns, 'Peptide')),
                    'ModifiedPeptide': ('PQM', idx_values(first_result.columns, 'ModifiedPeptide')),
                    'Charge': ('PQM', idx_values(first_result.columns, 'Charge')),
                },
            )

        # ---
        # fill in the dataset with dataarrays for each key (Intensity, RT etc...)
        for key in data_keys:
            pandas_result_merged = pandas_dict_merged[key]

            if pandas_result_merged is None:
                dataset[key.value] = xr.DataArray([])  # len(dataset[key.value]==0)
            else:
                dataset[key.value] = ((self.INDEX_NAME, 'PQM'), pandas_result_merged.values)

        return dataset


def set_stream_logger(name='pipeline', level=logging.INFO, format_string=None, fname=None):
    """
    Add a stream handler for the given name and level to the logging module.
    By default, this logs all boto3 messages to ``stdout``.

    :type name: string
    :param name: Log name
    :type level: int
    :param level: Logging level, e.g. ``logging.INFO``
    :type format_string: str
    :param format_string: Log message format
    """
    logger = logging.getLogger(name)
    logger.setLevel(level)

    # clear handlers
    logger.handlers = []

    if format_string is None:
        format_string = '%(asctime)s [%(levelname)s] {%(filename)s:%(funcName)s:%(lineno)d} | %(message)s'
    formatter = logging.Formatter(format_string)

    # set up the screen logging
    handler = logging.StreamHandler()
    handler.setLevel(level)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # create file handler which logs even debug messages
    if fname is not None:
        file_handler = logging.FileHandler(fname)
        file_handler.setLevel(level)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    return logger


if __name__ == '__main__':
    main()
