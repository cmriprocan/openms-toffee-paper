# coding: utf-8
import glob
import os
import shutil
import zipfile

import pandas as pd

files = glob.glob('Spyogenes/raw/*zip')
metadata = []
for f in files:
    bn = os.path.basename(f)
    exp, rep = bn.replace('%', '').split('_')[2:4]
    result = {
        'File': bn,
        'Condition': 'Strep0' if 'p0Pla' in exp else 'Strep10',
        'BiologicalReplicate': 1 if 'Repl1' in exp else 2,
        'TechnicalReplicate': int(rep[1:]),
    }
    metadata.append(result)
    
metadata = pd.DataFrame(metadata)
metadata['InjectionName'] = metadata.Condition + 'Repl' + metadata.BiologicalReplicate.map(str) + '_R0' + metadata.TechnicalReplicate.map(str) + '_SW'
metadata.sort_values('InjectionName', inplace=True)
metadata.to_csv('roest2016-metadata.tsv', sep='\t')

for _, row in metadata.iterrows():
    print(row.InjectionName)
    with zipfile.ZipFile(f'Spyogenes/raw/{row.File}', 'r') as zip_ref:
        zip_ref.extractall(f'Spyogenes/raw/extract/{row.InjectionName}')

wiff_files = glob.glob('Spyogenes/raw/extract/**/*.wiff', recursive=True)
for w in wiff_files:
    bn, ext = os.path.splitext(os.path.basename(w))
    outname = metadata[metadata.File == bn + '.zip'].InjectionName.iloc[0] + ext
    for ext in ['', '.scan']:
        shutil.move(w + ext, 'wiff/' + outname + ext)

