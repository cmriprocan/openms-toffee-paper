import argparse
import os
import shutil
import sys
from subprocess import check_call

import pandas as pd


def main():
    parser = argparse.ArgumentParser(
        description='Convert the raw data',
    )
    parser.add_argument(
        'method',
        type=str,
        help='The conversion method (e.g. msconvert, sciex, toffee, toffee_mzml, mz5)',
    )
    parser.add_argument(
        'experiment',
        type=str,
        help='The name of the experiment (e.g. sgs, roest2016, procan90)',
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='Switch on debugging',
    )
    args = parser.parse_args()

    if args.method == 'msconvert':
        base_outdir = 'Z:/E0004_Data-SOPs/E0004-P02_file-format/SGS/raw_data'
    else:
        base_outdir = os.getcwd()

    if args.experiment == 'sgs':
        base_outdir += '/sgs'
        metadata = load_sgs_metadata(f'{base_outdir}/sgs-metadata.tsv')
    elif args.experiment == 'roest2016':
        base_outdir += '/roest2016'
        metadata = load_roest2016_metadata(f'{base_outdir}/roest2016-metadata.tsv')
    elif args.experiment == 'procan90':
        base_outdir += '/procan90'
        metadata = pd.DataFrame([{'InjectionName': 'ProCan90-M0{}-01'.format(m), 'Machine': m} for m in range(1, 7)])
    else:
        raise ValueError(f'Invalid experiment: {args.experiment}')

    if args.method == 'msconvert':
        if sys.platform != 'win32':
            raise ValueError('mscovert only works on Windows')
        msconvert(base_outdir, metadata)
    elif args.method == 'sciex':
        mzml_docker(base_outdir, metadata)
    elif args.method == 'toffee':
        tof_docker(base_outdir, metadata)
    elif args.method == 'toffee_mzml':
        tof_mzml_docker(base_outdir, metadata)
    elif args.method == 'mz5':
        mz5_docker(base_outdir, metadata)
    else:
        raise ValueError(f'Invalid method: {args.method}')


def load_sgs_metadata(fname):
    df = pd.read_csv(fname, sep='\t')
    df = df[(df.Background == 'Human') & (df.TechnicalReplicate == 1)]
    df = df.sort_values('Dilution', ascending=False).reset_index()
    return df


def load_roest2016_metadata(fname):
    df = pd.read_csv(fname, sep='\t')
    df = df.sort_values('InjectionName').reset_index()
    return df


def msconvert(base_outdir, metadata):
    exe = 'C:/Users/btully.CMRI/Documents/pwiz-toffee/build-nt-x86/msvc-release-x86_64/msconvert.exe'

    commands = {
        'a': [
            exe,
            '--mzML',
            '-z',
        ],
        'b': [
            exe,
            '--mzML',
            '-z',
            '--numpressLinear',
        ],
        'c': [
            exe,
            '--mzML',
            '-z',
            '--filter',
            '"peakPicking vendor"',
        ],
        'd': [
            exe,
            '--mzML',
            '-z',
            '--numpressLinear',
            '--filter',
            '"peakPicking vendor"',
        ],
    }

    for i, row in metadata.iterrows():
        print(i, 'of', metadata.shape[0], '||', row.InjectionName)
        for key, base_cmd in commands.items():
            local_outfname = f'{row.InjectionName}.{key}.mzML'
            outdir = f'{base_outdir}/mzML/{key}'
            outfname = f'{outdir}/{local_outfname}'

            if os.path.isfile(outfname):
                continue

            if not os.path.isfile(local_outfname):
                cmd = base_cmd + [
                    '--outfile',
                    local_outfname,
                    f'{base_outdir}/wiff/{row.InjectionName}.wiff',
                ]
                check_call(cmd)

            os.makedirs(outdir, exist_ok=True)
            shutil.move(local_outfname, outfname)


def mzml_docker(base_outdir, metadata):
    import docker

    volumes = {}

    def add_volume(d, docker_d):
        if d not in volumes:
            volumes[d] = {'bind': docker_d, 'mode': 'rw'}
        return volumes[d]['bind']

    wiff_docker_dir = add_volume(f'{base_outdir}/wiff', '/wiff')
    mzml_basedir = f'{base_outdir}/mzML'
    mzml_docker_dir = add_volume(mzml_basedir, '/mzML')

    docker_client = docker.from_env()
    docker_image = 'sciex/wiffconverter:0.9'

    base_command = [
        'mono',
        '/usr/local/bin/sciex/wiffconverter/OneOmics.WiffConverter.exe',
        'WIFF',
        wiff_docker_dir + '/{name}.wiff',
        '{method}',
        'MZML',
        mzml_docker_dir + '/{key}/{name}.{key}.mzML',
        '--zlib',
        '--index',
    ]
    base_command = ' '.join(base_command)

    methods = {
        'e': '-profile',
        'f': '-centroid',
    }

    for i, row in metadata.iterrows():
        print(i, 'of', metadata.shape[0], '||', row.InjectionName)
        for key, method in methods.items():

            mzml_dir = f'{mzml_basedir}/{key}'
            os.makedirs(mzml_dir, exist_ok=True)

            command = base_command.format(
                name=row.InjectionName,
                method=method,
                key=key,
            )
            container = docker_client.containers.run(
                image=docker_image,
                command=command,
                volumes=volumes,
                detach=True,
            )
            exit_status_dict = container.wait()
            exit_status = exit_status_dict['StatusCode']
            output = container.logs(
                stdout=True,
                stderr=True,
                stream=False,
            ).decode('utf-8')
            container.remove(force=True)

            if exit_status != 0:
                raise docker.errors.ContainerError(
                    container,
                    exit_status,
                    command,
                    docker_image,
                    output,
                )


def tof_docker(base_outdir, metadata):
    import docker

    volumes = {}

    def add_volume(d, docker_d):
        if d not in volumes:
            volumes[d] = {'bind': docker_d, 'mode': 'rw'}
        return volumes[d]['bind']

    mzml_docker_dir = add_volume(f'{base_outdir}/mzML', '/mzML')
    tof_basedir = f'{base_outdir}/tof'
    tof_docker_dir = add_volume(tof_basedir, '/tof')

    docker_client = docker.from_env()
    docker_image = 'cmriprocan/toffee:0.12.16'

    # ---
    # mzML to Toffee
    mzml_to_tof_keys = ['a', 'e']  # msconvert + sciex profile
    for i, row in metadata.iterrows():
        for key in mzml_to_tof_keys:
            print(i, 'of', metadata.shape[0], '||', row.InjectionName, ' || mzml_to_toffee ||', key)

            # make output directories
            tof_dir = f'{tof_basedir}/{key}'
            os.makedirs(tof_dir, exist_ok=True)

            tof_basename = f'{row.InjectionName}.{key}.tof'
            command = [
                'mzml_to_toffee',
                f'{mzml_docker_dir}/{key}/{row.InjectionName}.{key}.mzML',
                f'{tof_docker_dir}/{key}/{tof_basename}',
            ]
            command = ' '.join(command)

            tof_path = f'{tof_dir}/{tof_basename}'
            if os.path.exists(tof_path):
                continue

            container = docker_client.containers.run(
                image=docker_image,
                command=command,
                volumes=volumes,
                detach=True,
            )
            exit_status_dict = container.wait()
            exit_status = exit_status_dict['StatusCode']
            output = container.logs(
                stdout=True,
                stderr=True,
                stream=False,
            ).decode('utf-8')
            container.remove(force=True)

            if exit_status != 0:
                raise docker.errors.ContainerError(
                    container,
                    exit_status,
                    command,
                    docker_image,
                    output,
                )


def tof_mzml_docker(base_outdir, metadata):
    import docker

    volumes = {}

    def add_volume(d, docker_d):
        if d not in volumes:
            volumes[d] = {'bind': docker_d, 'mode': 'rw'}
        return volumes[d]['bind']

    tof_basedir = f'{base_outdir}/tof'
    mzml_basedir = f'{base_outdir}/mzML'
    mzml_docker_dir = add_volume(mzml_basedir, '/mzML')
    tof_docker_dir = add_volume(tof_basedir, '/tof')

    docker_client = docker.from_env()
    docker_image = 'cmriprocan/toffee:0.12.16'

    # ---
    # mzML to Toffee
    tof_key = 'e'  # sciex toffee file
    mzml_key = 'g'
    for i, row in metadata.iterrows():
        print(i, 'of', metadata.shape[0], '||', row.InjectionName, ' || toffee_to_mzml')

        # make output directories
        tof_dir = f'{tof_basedir}/{tof_key}'
        mzml_dir = f'{mzml_basedir}/{mzml_key}'
        os.makedirs(tof_dir, exist_ok=True)
        os.makedirs(mzml_dir, exist_ok=True)

        tof_basename = f'{row.InjectionName}.{tof_key}.tof'
        mzml_basename = f'{row.InjectionName}.{mzml_key}.mzML'

        command = [
            'toffee_to_mzml',
            f'{tof_docker_dir}/{tof_key}/{tof_basename}',
            f'{mzml_docker_dir}/{mzml_key}/{mzml_basename}',
        ]
        command = ' '.join(command)

        mzml_path = f'{mzml_dir}/{mzml_basename}'
        if os.path.exists(mzml_path):
            continue

        container = docker_client.containers.run(
            image=docker_image,
            command=command,
            volumes=volumes,
            detach=True,
        )
        exit_status_dict = container.wait()
        exit_status = exit_status_dict['StatusCode']
        output = container.logs(
            stdout=True,
            stderr=True,
            stream=False,
        ).decode('utf-8')
        container.remove(force=True)

        if exit_status != 0:
            raise docker.errors.ContainerError(
                container,
                exit_status,
                command,
                docker_image,
                output,
            )


def mz5_docker(base_outdir, metadata):
    import docker

    volumes = {}

    def add_volume(d, docker_d):
        if d not in volumes:
            volumes[d] = {'bind': docker_d, 'mode': 'rw'}
        return volumes[d]['bind']

    wiff_dir = f'{base_outdir}/wiff'
    mz5_dir = f'{base_outdir}/mz5'
    wiff_docker_dir = add_volume(wiff_dir, '/wiff')
    mz5_docker_dir = add_volume(mz5_dir, '/mz5')

    docker_client = docker.from_env()
    docker_image = 'chambm/pwiz-skyline-i-agree-to-the-vendor-licenses:3.0.19073-85be84641'

    # ---
    # mzML to Toffee
    for i, row in metadata.iterrows():
        print(i, 'of', metadata.shape[0], '||', row.InjectionName, ' || wiff_to_mz5')

        # make output directories
        os.makedirs(mz5_dir, exist_ok=True)

        wiff_basename = f'{row.InjectionName}.wiff'
        mz5_basename = f'{row.InjectionName}.mz5'

        command = [
            'wine',
            'msconvert',
            f'-o {mz5_docker_dir}',
            f'--outfile {mz5_basename}',
            '-z --mz5',
            f'{wiff_docker_dir}/{wiff_basename}',
        ]
        command = ' '.join(command)

        mz5_path = f'{mz5_dir}/{mz5_basename}'
        if os.path.exists(mz5_path):
            continue

        container = docker_client.containers.run(
            image=docker_image,
            command=command,
            volumes=volumes,
            detach=True,
        )
        exit_status_dict = container.wait()
        exit_status = exit_status_dict['StatusCode']
        output = container.logs(
            stdout=True,
            stderr=True,
            stream=False,
        ).decode('utf-8')
        container.remove(force=True)

        if exit_status != 0:
            raise docker.errors.ContainerError(
                container,
                exit_status,
                command,
                docker_image,
                output,
            )


if __name__ == '__main__':
    main()
