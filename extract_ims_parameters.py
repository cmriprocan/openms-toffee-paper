# coding: utf-8
import os

import h5py

import matplotlib.pyplot as plt

import pandas as pd

import seaborn as sns

sns.set()


def main():
    result = pd.concat([
        get_ims_parameters('sgs', 'sgs'),
        # get_ims_parameters('roest2016', 'roest2016'),
    ])

    g = sns.catplot(
        kind='strip',
        data=result,
        x='Dilution',
        y='Value',
        hue='Mode',
        col='Dataset',
        row='Type',
        jitter=True,
        # dodge=True,
        sharey=False,
        palette='viridis',
    )
    for ax in g.axes[0, :]:
        ax.set_ylim([7.05685e-5, 7.0570e-5])
    for ax in g.axes[1, :]:
        ax.set_ylim([-5e-5, 5e-5])

    plt.show()


def get_ims_parameters(basedir, basename):
    metadata = pd.read_csv(f'{basedir}/{basename}-metadata.tsv', sep='\t')
    result = []
    for _, row in metadata.iterrows():
        fname = f'{basedir}/tof/{row.InjectionName}.tof'
        if basename == 'sgs':
            fname = fname.replace('_SW', '_sw')
        if not os.path.isfile(fname):
            continue
        with h5py.File(fname, 'r') as f:
            for key in f.keys():
                a = f[key].attrs
                for k, v in a.items():
                    if k.startswith('IMS'):
                        r = {
                            'InjectionName': row.InjectionName,
                            'Mode': 'MS1' if key == 'ms1' else 'MS2',
                            'Window': key,
                            'Type': k,
                            'Value': v,
                            'Dataset': 'SGS' if basedir == 'sgs' else 'Roest2016',
                        }
                        result.append(r)

    assert len(result) > 0
    result = pd.merge(pd.DataFrame(result), metadata, on='InjectionName')
    return result


if __name__ == '__main__':
    main()
